import pandas as pd
from datetime import datetime, timedelta

from strategies import *


def main():
    df_hi = pd.read_excel('./hi1 es1 1min ms 20200527.xlsx', sheet_name='hi1')
    df_es = pd.read_excel('./hi1 es1 1min ms 20200527.xlsx', sheet_name='es1')

    df_hi.index = pd.to_datetime(df_hi['Dates'])

    # add is False when it is used to backtest strategy 3.
    add = True
    if add:
        # Add 13 hours to be HKT.
        df_es.index = pd.to_datetime(df_es.loc[:, 'Dates'] + timedelta(hours=13))
    else:
        df_es.index = pd.to_datetime(df_es['Dates'])

    df_es.drop_duplicates(keep=False, inplace=True)
    df_hi.drop_duplicates(keep=False, inplace=True)
    df_es = df_es.sort_index()
    df_hi = df_hi.sort_index()

    ernest_modified_strategy_1(df_es, lookback_period=21, z_score=1)


if __name__ == '__main__':
    main()
