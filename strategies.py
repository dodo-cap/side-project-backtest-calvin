import pandas as pd
from datetime import datetime, timedelta
import seaborn as sns
import matplotlib.pyplot as plt
from contextlib import closing
from multiprocessing import Pool
from matplotlib import style

import backtest_utilities as bu

style.use('seaborn')


# PROJECT_DIR = os.path.abspath(__file__)[:os.path.abspath(__file__).find('nlp-sentiment') + 13]
def parallel_helper(func, arg_list, worker_count=8):
    """
    Behave exactly the same as map
    :param func:
    :param arg_list:
    :param worker_count:
    :return:
    """
    with closing(Pool(processes=worker_count)) as p:
        ret = p.map(func, arg_list)
        p.terminate()
    return ret


def strategy_1(df, entry_at_open=True):
    # 1. 0600 HKT long es till 2230 HKT

    if entry_at_open:
        entry = 'Open'
    else:
        entry = 'Close'

    es = df.copy()
    # es = es.between_time(start_time='06:00:00', end_time='22:30:00', include_start=True, include_end=True)
    # The below 1 line code does NOT work.
    # strategy_one_df = pd.Series(0.0, index=sorted(set(es.index.date)))

    d = {}
    entry_level = 0
    for i in range(es.__len__()):

        if es.index[i].hour == 6 and es.index[i].minute == 0 and not (pd.isna(es.iloc[i][entry])):
            entry_level = i
            # print(es.index[entry_level].date())

        if es.index[i].hour == 22 and es.index[i].minute == 30 and not (pd.isna(es.iloc[i]['Close'])):
            exit_level = i
            # print(es.index[exit_level].date())

            # if es.index[entry_level].date() == es.index[exit_level].date():

            profit_and_loss = es.iloc[exit_level]['Close'] / es.iloc[entry_level][entry] - 1
            print(f'Date: {es.index[entry_level].date()} Pnl: {profit_and_loss}')
            d[f'{es.index[entry_level].date()}'] = profit_and_loss

            # else:
            #     continue

    strategy_df = pd.Series(d)
    strategy_df.dropna(inplace=True)
    pf = bu.PerformanceAnalysis(pnl=strategy_df, starting_capital=10000)
    pf.performance_plot()
    # pf.performance_plot(save_fig=True, save_path='./backtest_charts/strategy1_entry_close_exit_close')

    return


def strategy_2(df, long=True, entry_at_open=True):
    # 2. 1200 HKT long es till 1300 HKT and 1900 HKT short es till 2000 HKT

    if entry_at_open:
        entry = 'Open'
    else:
        entry = 'Close'

    es = df.copy()
    long_es = es.between_time(start_time='12:00:00', end_time='13:00:00', include_start=True, include_end=True)
    short_es = es.between_time(start_time='19:00:00', end_time='20:00:00', include_start=True, include_end=True)
    if long:
        es = long_es
        d = {}
        entry_level = 0
        for i in range(es.__len__()):

            if es.index[i].hour == 12 and es.index[i].minute == 0 and not(pd.isna(es.iloc[i][entry])):
                entry_level = i

            if es.index[i].hour == 13 and es.index[i].minute == 0 and not(pd.isna(es.iloc[i]['Close'])):
                exit_level = i

                if es.index[entry_level].date() == es.index[exit_level].date():

                    profit_and_loss = es.iloc[exit_level]['Close'] / es.iloc[entry_level][entry] - 1
                    print(f'Date: {es.index[entry_level].date()} Pnl: {profit_and_loss}')
                    d[f'{es.index[entry_level].date()}'] = profit_and_loss

                else:
                    continue

        strategy_df = pd.Series(d)
        pf = bu.PerformanceAnalysis(pnl=strategy_df, starting_capital=10000)
        pf.performance_plot()

    else:
        es = short_es
        d = {}
        entry_level = 0
        for i in range(es.__len__()):

            if es.index[i].hour == 19 and es.index[i].minute == 0 and not(pd.isna(es.iloc[i][entry])):
                entry_level = i

            if es.index[i].hour == 20 and es.index[i].minute == 0 and not(pd.isna(es.iloc[i]['Close'])):
                exit_level = i

                if es.index[entry_level].date() == es.index[exit_level].date():
                    profit_and_loss = 1 - es.iloc[exit_level]['Close'] / es.iloc[entry_level][entry]
                    print(f'Date: {es.index[entry_level].date()} Pnl: {profit_and_loss}')
                    d[f'{es.index[entry_level].date()}'] = profit_and_loss

                else:
                    continue

        strategy_df = pd.Series(d)
        pf = bu.PerformanceAnalysis(pnl=strategy_df, starting_capital=10000)
        pf.performance_plot()
        # pf.performance_plot(save_fig=True, save_path='./backtest_charts/strategy2_short_at_1900')

    return


def strategy_3(df_hi, df_es, long=True, short=True, early_start=True, backtest='hi'):
    # 3. observe first 2 bars (5min) every day, if 2nd bar close > 1st bar high, long till day end;
    # if 2nd bar close < 1st bar low, short till day end
    if backtest == 'hi':
        if early_start:
            t = 15
        else:
            t = 30
        hi = df_hi.copy()
        hi = hi.between_time(start_time='09:15:00', end_time='16:29:00', include_start=True, include_end=True)
        conversion = {'Dates': 'last', 'Open': 'first', 'Close': 'last', 'High': 'max', 'Low': 'min'}
        hi = hi.resample('5Min').agg(conversion)
        l, s = 0, 0
        d = {}
        entry_level = 0
        for i in range(hi.__len__()-1):
            if long and short:

                if hi.index[i].hour == 9 and hi.index[i].minute == t \
                        and hi.index[i + 1].hour == 9 and hi.index[i + 1].minute == (t + 5) \
                        and hi.index[i].date() == hi.index[i + 1].date() and \
                        not(pd.isna(hi.iloc[i + 1]['Close'])):

                    if not(pd.isna(hi.iloc[i]['High'])) and hi.iloc[i + 1]['Close'] > hi.iloc[i]['High']:
                        entry_level = i + 1
                        l = 1

                    if not(pd.isna(hi.iloc[i]['High'])) and hi.iloc[i + 1]['Close'] < hi.iloc[i]['Low']:
                        entry_level = i + 1
                        s = 1

                if hi.iloc[i]['Dates'].hour == 16 and hi.iloc[i]['Dates'].minute == 29 and \
                        not(pd.isna(hi.iloc[i]['Close'])):
                    exit_level = i

                    if hi.index[entry_level].date() == hi.index[exit_level].date():
                        if l == 1:
                            profit_and_loss = hi.iloc[exit_level]['Close'] / hi.iloc[entry_level]['Close'] - 1
                            print(f'Date: {hi.index[entry_level].date()} Pnl: {profit_and_loss}')
                            d[f'{hi.index[entry_level].date()}'] = profit_and_loss
                            l = 0
                        if s == 1:
                            profit_and_loss = 1 - hi.iloc[exit_level]['Close'] / hi.iloc[entry_level]['Close']
                            print(f'Date: {hi.index[entry_level].date()} Pnl: {profit_and_loss}')
                            d[f'{hi.index[entry_level].date()}'] = profit_and_loss
                            s = 0

                else:
                    continue

            if long and not short:
                if hi.index[i].hour == 9 and hi.index[i].minute == t \
                        and hi.index[i + 1].hour == 9 and hi.index[i + 1].minute == (t + 5) \
                        and hi.index[i].date() == hi.index[i + 1].date() and \
                        not (pd.isna(hi.iloc[i + 1]['Close']) or pd.isna(hi.iloc[i]['High'])):

                    if hi.iloc[i + 1]['Close'] > hi.iloc[i]['High']:
                        entry_level = i + 1

                if hi.iloc[i]['Dates'].hour == 16 and hi.iloc[i]['Dates'].minute == 29 and \
                        not(pd.isna(hi.iloc[i]['Close'])):
                    exit_level = i

                    if hi.index[entry_level].date() == hi.index[exit_level].date():
                        profit_and_loss = hi.iloc[exit_level]['Close'] / hi.iloc[entry_level]['Close'] - 1
                        print(f'Date: {hi.index[entry_level].date()} Pnl: {profit_and_loss}')
                        d[f'{hi.index[entry_level].date()}'] = profit_and_loss

                else:
                    continue

            if short and not long:
                if hi.index[i].hour == 9 and hi.index[i].minute == t \
                        and hi.index[i+1].hour == 9 and hi.index[i+1].minute == (t + 5) \
                        and hi.index[i].date() == hi.index[i+1].date() and \
                        not(pd.isna(hi.iloc[i+1]['Close']) or pd.isna(hi.iloc[i]['Low'])):

                    if hi.iloc[i+1]['Close'] < hi.iloc[i]['Low']:
                        entry_level = i+1

                if hi.iloc[i]['Dates'].hour == 16 and hi.iloc[i]['Dates'].minute == 29 and \
                        not(pd.isna(hi.iloc[i]['Close'])):
                    exit_level = i

                    if hi.index[entry_level].date() == hi.index[exit_level].date():
                        profit_and_loss = 1 - hi.iloc[exit_level]['Close'] / hi.iloc[entry_level]['Close']
                        print(f'Date: {hi.index[entry_level].date()} Pnl: {profit_and_loss}')
                        d[f'{hi.index[entry_level].date()}'] = profit_and_loss

                else:
                    continue

        strategy_df = pd.Series(d)
        pf = bu.PerformanceAnalysis(pnl=strategy_df, starting_capital=10000)
        pf.performance_plot()
        # pf.performance_plot(save_fig=True, save_path='./backtest_charts/strategy3/strategy3_long_short_hi_0940_to_1630.png')

        return

    elif backtest == 'es':
        es = df_es.copy()

        es_daylight_saving_before = es[es.index < '2020-03-09']
        es_daylight_saving_after = es[es.index >= '2020-03-09']

        before = es_daylight_saving_before.copy()
        after = es_daylight_saving_after.copy()

        before = before.between_time(start_time='08:30:00', end_time='14:59:00', include_start=True, include_end=True)
        after = after.between_time(start_time='09:30:00', end_time='15:59:00', include_start=True, include_end=True)
        conversion = {'Dates': 'last', 'Open': 'first', 'Close': 'last', 'High': 'max', 'Low': 'min'}

        before = before.resample('5Min').agg(conversion)
        after = after.resample('5Min').agg(conversion)
        d = {}

        if long and short:
            l, s = 0, 0
            entry_level = 0

            for i in range(before.__len__()-1):
                if before.index[i].hour == 8 and before.index[i].minute == 30 \
                        and before.index[i + 1].hour == 8 and before.index[i + 1].minute == 35 \
                        and before.index[i].date() == before.index[i + 1].date() and \
                        not (pd.isna(before.iloc[i + 1]['Close'])):

                    if not (pd.isna(before.iloc[i]['High'])) and before.iloc[i + 1]['Close'] > before.iloc[i]['High']:
                        entry_level = i + 1
                        l = 1

                    if not(pd.isna(before.iloc[i]['High'])) and before.iloc[i + 1]['Close'] < before.iloc[i]['Low']:
                        entry_level = i + 1
                        s = 1

                if before.iloc[i]['Dates'].hour == 14 and before.iloc[i]['Dates'].minute == 59 and \
                        not(pd.isna(before.iloc[i]['Close'])):
                    exit_level = i

                    if before.index[entry_level].date() == before.index[exit_level].date():
                        if l == 1:
                            profit_and_loss = before.iloc[exit_level]['Close'] / before.iloc[entry_level]['Close'] - 1
                            print(f'Date: {before.index[entry_level].date()} Pnl: {profit_and_loss}')
                            d[f'{before.index[entry_level].date()}'] = profit_and_loss
                            l = 0
                        if s == 1:
                            profit_and_loss = 1 - before.iloc[exit_level]['Close'] / before.iloc[entry_level]['Close']
                            print(f'Date: {before.index[entry_level].date()} Pnl: {profit_and_loss}')
                            d[f'{before.index[entry_level].date()}'] = profit_and_loss
                            s = 0

                else:
                    continue

            entry_level = 0
            l, s = 0, 0
            for i in range(after.__len__() - 1):

                if after.index[i].hour == 9 and after.index[i].minute == 30 \
                        and after.index[i + 1].hour == 9 and after.index[i + 1].minute == 35 \
                        and after.index[i].date() == after.index[i + 1].date() and \
                        not (pd.isna(after.iloc[i + 1]['Close'])):

                    if not (pd.isna(after.iloc[i]['High'])) and after.iloc[i + 1]['Close'] > after.iloc[i]['High']:
                        entry_level = i + 1
                        l = 1

                    if not (pd.isna(after.iloc[i]['High'])) and after.iloc[i + 1]['Close'] < after.iloc[i]['Low']:
                        entry_level = i + 1
                        s = 1

                if after.iloc[i]['Dates'].hour == 15 and after.iloc[i]['Dates'].minute == 59 and \
                        not (pd.isna(after.iloc[i]['Close'])):
                    exit_level = i

                    if after.index[entry_level].date() == after.index[exit_level].date():
                        if l == 1:
                            profit_and_loss = after.iloc[exit_level]['Close'] / after.iloc[entry_level]['Close'] - 1
                            print(f'Date: {after.index[entry_level].date()} Pnl: {profit_and_loss}')
                            d[f'{after.index[entry_level].date()}'] = profit_and_loss
                            l = 0
                        if s == 1:
                            profit_and_loss = 1 - after.iloc[exit_level]['Close'] / after.iloc[entry_level]['Close']
                            print(f'Date: {after.index[entry_level].date()} Pnl: {profit_and_loss}')
                            d[f'{after.index[entry_level].date()}'] = profit_and_loss
                            s = 0

                else:
                    continue

        if long and not short:
            entry_level = 0

            for i in range(before.__len__() - 1):
                if before.index[i].hour == 8 and before.index[i].minute == 30 \
                        and before.index[i + 1].hour == 8 and before.index[i + 1].minute == 35 \
                        and before.index[i].date() == before.index[i + 1].date() and \
                        not (pd.isna(before.iloc[i + 1]['Close'])):

                    if not (pd.isna(before.iloc[i]['High'])) and before.iloc[i + 1]['Close'] > before.iloc[i]['High']:
                        entry_level = i + 1

                if before.iloc[i]['Dates'].hour == 14 and before.iloc[i]['Dates'].minute == 59 and \
                        not (pd.isna(before.iloc[i]['Close'])):
                    exit_level = i

                    if before.index[entry_level].date() == before.index[exit_level].date():
                        profit_and_loss = before.iloc[exit_level]['Close'] / before.iloc[entry_level]['Close'] - 1
                        print(f'Date: {before.index[entry_level].date()} Pnl: {profit_and_loss}')
                        d[f'{before.index[entry_level].date()}'] = profit_and_loss

                else:
                    continue

            entry_level = 0
            for i in range(after.__len__() - 1):

                if after.index[i].hour == 9 and after.index[i].minute == 30 \
                        and after.index[i + 1].hour == 9 and after.index[i + 1].minute == 35 \
                        and after.index[i].date() == after.index[i + 1].date() and \
                        not (pd.isna(after.iloc[i + 1]['Close'])):

                    if not (pd.isna(after.iloc[i]['High'])) and after.iloc[i + 1]['Close'] > after.iloc[i]['High']:
                        entry_level = i + 1

                if after.iloc[i]['Dates'].hour == 15 and after.iloc[i]['Dates'].minute == 59 and \
                        not (pd.isna(after.iloc[i]['Close'])):
                    exit_level = i

                    if after.index[entry_level].date() == after.index[exit_level].date():
                        profit_and_loss = after.iloc[exit_level]['Close'] / after.iloc[entry_level]['Close'] - 1
                        print(f'Date: {after.index[entry_level].date()} Pnl: {profit_and_loss}')
                        d[f'{after.index[entry_level].date()}'] = profit_and_loss

                else:
                    continue

        if short and not long:
            entry_level = 0

            for i in range(before.__len__() - 1):
                if before.index[i].hour == 8 and before.index[i].minute == 30 \
                        and before.index[i + 1].hour == 8 and before.index[i + 1].minute == 35 \
                        and before.index[i].date() == before.index[i + 1].date() and \
                        not (pd.isna(before.iloc[i + 1]['Close'])):

                    if not (pd.isna(before.iloc[i]['High'])) and before.iloc[i + 1]['Close'] < before.iloc[i]['Low']:
                        entry_level = i + 1

                if before.iloc[i]['Dates'].hour == 14 and before.iloc[i]['Dates'].minute == 59 and \
                        not (pd.isna(before.iloc[i]['Close'])):
                    exit_level = i

                    if before.index[entry_level].date() == before.index[exit_level].date():
                        profit_and_loss = 1 - before.iloc[exit_level]['Close'] / before.iloc[entry_level]['Close']
                        print(f'Date: {before.index[entry_level].date()} Pnl: {profit_and_loss}')
                        d[f'{before.index[entry_level].date()}'] = profit_and_loss
                else:
                    continue

            entry_level = 0
            for i in range(after.__len__() - 1):

                if after.index[i].hour == 9 and after.index[i].minute == 30 \
                        and after.index[i + 1].hour == 9 and after.index[i + 1].minute == 35 \
                        and after.index[i].date() == after.index[i + 1].date() and \
                        not (pd.isna(after.iloc[i + 1]['Close'])):

                    if not (pd.isna(after.iloc[i]['High'])) and after.iloc[i + 1]['Close'] < after.iloc[i]['Low']:
                        entry_level = i + 1

                if after.iloc[i]['Dates'].hour == 15 and after.iloc[i]['Dates'].minute == 59 and \
                        not (pd.isna(after.iloc[i]['Close'])):
                    exit_level = i

                    if after.index[entry_level].date() == after.index[exit_level].date():
                        profit_and_loss = 1 - after.iloc[exit_level]['Close'] / after.iloc[entry_level]['Close']
                        print(f'Date: {after.index[entry_level].date()} Pnl: {profit_and_loss}')
                        d[f'{after.index[entry_level].date()}'] = profit_and_loss

                else:
                    continue

        strategy_df = pd.Series(d)
        pf = bu.PerformanceAnalysis(pnl=strategy_df, starting_capital=10000)
        pf.performance_plot()
        # pf.performance_plot(save_fig=True, save_path='./backtest_charts/strategy3/strategy3_short_only_es.png')

        return


def modified_strategy_1(df, lookback_period=21, z_score=2):
    es = df.copy()
    es_signaling = es.between_time(start_time='22:30:00', end_time='06:00:00', include_start=True, include_end=True)
    es = es.between_time(start_time='06:00:00', end_time='22:30:00', include_start=True, include_end=True)

    ref = {}
    first, second = 0, 0
    for i in range(len(es_signaling)):

        if es_signaling.index[i].hour == 22 and es_signaling.index[i].minute == 30 \
                and not(pd.isna(es_signaling.iloc[i]['Open'])):
            first = i

        if es_signaling.index[i].hour == 6 and es_signaling.index[i].minute == 0 \
                and not(pd.isna(es_signaling.iloc[i]['Close'])):
            second = i

        if es_signaling.index[first].date() == es_signaling.index[second].date() - timedelta(days=1):
            # Use the second date as the key.
            ref[f'{es_signaling.index[second].date()}'] = \
                es_signaling.iloc[second]['Close'] / es_signaling.iloc[first]['Open'] - 1

    sig = pd.DataFrame.from_dict(ref, orient='index', columns=['change'])
    sig['ma'] = sig['change'].rolling(window=lookback_period).mean()
    sig['sd'] = sig['change'].rolling(window=lookback_period).std()
    sig['signal'] = (sig['change'] - sig['ma'])/sig['sd'] > z_score
    sig['signal_long'] = (sig['change'] - sig['ma'])/sig['sd'] < -z_score

    d = {}
    entry_level = 0
    for i in range(es.__len__()):

        if es.index[i].hour == 6 and es.index[i].minute == 0 and not (pd.isna(es.iloc[i]['Open'])):
            entry_level = i
            # print(es.index[entry_level].date())

        if es.index[i].hour == 22 and es.index[i].minute == 30 and not (pd.isna(es.iloc[i]['Close'])):
            exit_level = i
            # print(es.index[exit_level].date())

            if es.index[entry_level].date() == es.index[exit_level].date():
                try:
                    if sig.loc[f'{es.index[entry_level].date()}', 'signal']:
                        profit_and_loss = 1 - es.iloc[exit_level]['Close'] / es.iloc[entry_level]['Open']
                    # elif sig.loc[f'{es.index[entry_level].date()}', 'signal_long']:
                    #     profit_and_loss = es.iloc[exit_level]['Close'] / es.iloc[entry_level]['Open'] - 1
                    else:
                        profit_and_loss = es.iloc[exit_level]['Close'] / es.iloc[entry_level]['Open'] - 1
                    print(f'Date: {es.index[entry_level].date()} Pnl: {profit_and_loss}')
                    d[f'{es.index[entry_level].date()}'] = profit_and_loss
                except KeyError:
                    print(f'No this date: {es.index[entry_level].date()}')
            #
            # else:
            #     continue

    strategy_df = pd.Series(d)
    strategy_df.dropna(inplace=True)
    pf = bu.PerformanceAnalysis(pnl=strategy_df, starting_capital=10000)
    pf.performance_plot()

    return


def modified_strategy_1_heatmap(df):
    lookbacks = [3, 5, 10, 21, 42, 63]
    z_scores = [0.25, 0.5, 0.75, 1, 1.25, 1.5, 1.75, 2]
    # z_scores = [1.5, 1.75, 2]

    es = df.copy()
    es_signaling = es.between_time(start_time='22:30:00', end_time='06:00:00', include_start=True, include_end=True)
    es = es.between_time(start_time='06:00:00', end_time='22:30:00', include_start=True, include_end=True)

    sharpe_ratio_matrix = pd.DataFrame(index=z_scores, columns=lookbacks)
    # calmar_ratio_matrix = pd.DataFrame(index=z_scores, columns=lookbacks)

    for lb in lookbacks:
        for z_score in z_scores:
            ref = {}
            first, second = 0, 0
            for i in range(len(es_signaling)):

                if es_signaling.index[i].hour == 22 and es_signaling.index[i].minute == 30 \
                        and not(pd.isna(es_signaling.iloc[i]['Open'])):
                    first = i

                if es_signaling.index[i].hour == 6 and es_signaling.index[i].minute == 00 \
                        and not(pd.isna(es_signaling.iloc[i]['Close'])):
                    second = i

                if es_signaling.index[first].date() == es_signaling.index[second].date() - timedelta(days=1):
                    # Use the second date as the key.
                    ref[f'{es_signaling.index[second].date()}'] = \
                        es_signaling.iloc[second]['Close'] / es_signaling.iloc[first]['Open'] - 1

            lookback_period = lb
            sig = pd.DataFrame.from_dict(ref, orient='index', columns=['change'])
            sig['ma'] = sig['change'].rolling(window=lookback_period).mean()
            sig['sd'] = sig['change'].rolling(window=lookback_period).std()
            sig['signal'] = ((sig['change'] - sig['ma']) / sig['sd']) > z_score
            sig['signal_long'] = ((sig['change'] - sig['ma']) / sig['sd']) < -z_score

            d = {}
            entry_level = 0
            for i in range(es.__len__()):

                if es.index[i].hour == 6 and es.index[i].minute == 0 and not (pd.isna(es.iloc[i]['Open'])):
                    entry_level = i
                    # print(es.index[entry_level].date())

                if es.index[i].hour == 22 and es.index[i].minute == 30 and not (pd.isna(es.iloc[i]['Close'])):
                    exit_level = i
                    # print(es.index[exit_level].date())

                    # if es.index[entry_level].date() == es.index[exit_level].date():
                    try:
                        if sig.loc[f'{es.index[entry_level].date()}', 'signal']:
                            profit_and_loss = es.iloc[exit_level]['Close'] / es.iloc[entry_level]['Open'] - 1
                        elif sig.loc[f'{es.index[entry_level].date()}', 'signal_long']:
                            profit_and_loss = 1 - es.iloc[exit_level]['Close'] / es.iloc[entry_level]['Open']
                        else:
                            profit_and_loss = 0.0
                            # profit_and_loss = es.iloc[exit_level]['Close'] / es.iloc[entry_level]['Open'] - 1
                        print(f'Date: {es.index[entry_level].date()} Pnl: {profit_and_loss}')
                        d[f'{es.index[entry_level].date()}'] = profit_and_loss
                    except KeyError:
                        print(f'No this date: {es.index[entry_level].date()}')
                    # else:
                    #     continue

            strategy_df = pd.Series(d)
            strategy_df.dropna(inplace=True)
            pf = bu.PerformanceAnalysis(pnl=strategy_df, starting_capital=10000)

            sharpe_ratio_matrix.loc[z_score, lb] = pf.fetch_annualized_sharpe_ratio()
            # calmar_ratio_matrix.loc[z_score, lb] = pf.fetch_calmar_ratio()

    m = sns.heatmap(sharpe_ratio_matrix.astype('float64'), cmap='YlGn', annot=True)

    plt.ylabel('z scores')
    plt.xlabel('lookback windows')
    plt.show()


def ernest_modified_strategy_1(df, lookback_period=5, z_score=2):
    es = df.copy()
    es_signaling = es.between_time(start_time='22:30:00', end_time='06:00:00', include_start=True, include_end=True)
    es = es.between_time(start_time='06:00:00', end_time='22:30:00', include_start=True, include_end=True)

    ref = {}
    first, second = 0, 0
    for i in range(len(es_signaling)):

        if es_signaling.index[i].hour == 22 and es_signaling.index[i].minute == 30 \
                and not(pd.isna(es_signaling.iloc[i]['Open'])):
            first = i

        if es_signaling.index[i].hour == 5 and es_signaling.index[i].minute == 59 \
                and not(pd.isna(es_signaling.iloc[i]['Open'])):
            second = i

        if es_signaling.index[first].date() == es_signaling.index[second].date() - timedelta(days=1):
            # Use the second date as the key.
            ref[f'{es_signaling.index[second].date()}'] = \
                es_signaling.iloc[second]['Close'] / es_signaling.iloc[first]['Open'] - 1

    sig = pd.DataFrame.from_dict(ref, orient='index', columns=['change'])
    sig['ma'] = sig['change'].rolling(window=lookback_period).mean()
    sig['sd'] = sig['change'].rolling(window=lookback_period).std()
    sig['signal_long'] = (sig['change'] - sig['ma'])/sig['sd'] > z_score
    sig['signal_short'] = (sig['change'] - sig['ma'])/sig['sd'] < -z_score

    d = {}
    entry_level = 0
    for i in range(es.__len__()):

        if es.index[i].hour == 6 and es.index[i].minute == 0 and not (pd.isna(es.iloc[i]['Open'])):
            entry_level = i
            # print(es.index[entry_level].date())

        if es.index[i].hour == 22 and es.index[i].minute == 30 and not (pd.isna(es.iloc[i]['Close'])):
            exit_level = i
            # print(es.index[exit_level].date())

            if es.index[entry_level].date() == es.index[exit_level].date():
                if sig.loc[f'{es.index[entry_level].date()}', 'signal_long']:
                    profit_and_loss = es.iloc[exit_level]['Close'] / es.iloc[entry_level]['Open'] - 1
                elif sig.loc[f'{es.index[entry_level].date()}', 'signal_short']:
                    profit_and_loss = 1 - es.iloc[exit_level]['Close'] / es.iloc[entry_level]['Open']
                else:
                    profit_and_loss = 0.0
                print(f'Date: {es.index[entry_level].date()} Pnl: {profit_and_loss}')
                d[f'{es.index[entry_level].date()}'] = profit_and_loss

            else:
                continue

    strategy_df = pd.Series(d)
    strategy_df.dropna(inplace=True)
    pf = bu.PerformanceAnalysis(pnl=strategy_df, starting_capital=10000)
    pf.performance_plot()

    return


def ernest_modified_strategy_1_heatmap(df):
    lookbacks = [3, 5, 10, 21, 42, 63]
    z_scores = [0.25, 0.5, 0.75, 1, 1.25, 1.5, 1.75, 2]

    es = df.copy()
    es_signaling = es.between_time(start_time='22:30:00', end_time='06:00:00', include_start=True, include_end=True)
    es = es.between_time(start_time='06:00:00', end_time='22:30:00', include_start=True, include_end=True)

    sharpe_ratio_matrix = pd.DataFrame(index=z_scores, columns=lookbacks)
    # calmar_ratio_matrix = pd.DataFrame(index=z_scores, columns=lookbacks)

    for lb in lookbacks:
        for z_score in z_scores:
            ref = {}
            first, second = 0, 0
            for i in range(len(es_signaling)):

                if es_signaling.index[i].hour == 22 and es_signaling.index[i].minute == 30 \
                        and not(pd.isna(es_signaling.iloc[i]['Open'])):
                    first = i

                if es_signaling.index[i].hour == 6 and es_signaling.index[i].minute == 0 \
                        and not(pd.isna(es_signaling.iloc[i]['Close'])):
                    second = i

                if es_signaling.index[first].date() == es_signaling.index[second].date() - timedelta(days=1):
                    # Use the second date as the key.
                    ref[f'{es_signaling.index[second].date()}'] = \
                        es_signaling.iloc[second]['Close'] / es_signaling.iloc[first]['Open'] - 1

            lookback_period = lb
            sig = pd.DataFrame.from_dict(ref, orient='index', columns=['change'])
            sig['ma'] = sig['change'].rolling(window=lookback_period).mean()
            sig['sd'] = sig['change'].rolling(window=lookback_period).std()
            sig['signal_long'] = ((sig['change'] - sig['ma']) / sig['sd']) > z_score
            sig['signal_short'] = ((sig['change'] - sig['ma']) / sig['sd']) < -z_score

            d = {}
            entry_level = 0
            for i in range(es.__len__()):

                if es.index[i].hour == 6 and es.index[i].minute == 0 and not (pd.isna(es.iloc[i]['Open'])):
                    entry_level = i
                    # print(es.index[entry_level].date())

                if es.index[i].hour == 22 and es.index[i].minute == 30 and not (pd.isna(es.iloc[i]['Close'])):
                    exit_level = i
                    # print(es.index[exit_level].date())

                    if es.index[entry_level].date() == es.index[exit_level].date():
                        if sig.loc[f'{es.index[entry_level].date()}', 'signal_long']:
                            profit_and_loss = es.iloc[exit_level]['Close'] / es.iloc[entry_level]['Open'] - 1
                        elif sig.loc[f'{es.index[entry_level].date()}', 'signal_short']:
                            profit_and_loss = 1 - es.iloc[exit_level]['Close'] / es.iloc[entry_level]['Open']
                        else:
                            profit_and_loss = 0.0
                        print(f'Date: {es.index[entry_level].date()} Pnl: {profit_and_loss}')
                        d[f'{es.index[entry_level].date()}'] = profit_and_loss
                    else:
                        continue

            strategy_df = pd.Series(d)
            strategy_df.dropna(inplace=True)
            pf = bu.PerformanceAnalysis(pnl=strategy_df, starting_capital=10000)

            sharpe_ratio_matrix.loc[z_score, lb] = pf.fetch_annualized_sharpe_ratio()
            # calmar_ratio_matrix.loc[z_score, lb] = pf.fetch_calmar_ratio()

    m = sns.heatmap(sharpe_ratio_matrix.astype('float64'), cmap='YlGn', annot=True)

    plt.ylabel('z scores')
    plt.xlabel('lookback windows')
    plt.show()
