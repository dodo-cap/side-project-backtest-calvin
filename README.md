# Side Project Backtesting
## Strategies

### Strategy 1: time pattern1 on es
* 0600 HKT long es till 2230 HKT (done: 28/5)
* Case 1: Entry at Open; Case 2: Entry at Close

#### Modification:
* TWO backtest data handling: Include
* 如果2230-0600升超過2sd, 就0600-2230改為short (Done: 31/5)
* If 2230-0600 N-day Z-score > threshold -> LONG 0600-2230; vice verse
* If 0600-2230 > 2SD, short next 0600-2230, otherwise long.

### Strategy 2: time pattern2 on es
* 1200 HKT long es till 1300 HKT (done: 28/5) 
* 1900 HKT short es till 2000 HKT (done: 28/5)
* Case 1: Entry at Open; Case 2: Entry at Close

#### Conversion:
* HKT (UTC + 8) VS CT (UTC - 5) : adding 13 hours converting to HKT
* HKT: 1200 to 1300 <=> CT: 23:00 - 00:00
* HKT: 1900 to 2000 <=> CT: 06:00 - 07:00

#### Modification:
* add backtest on Long ES at Open and exit at Close at 1200 HKT for that 1 min (done: 28/5)
* add backtest on Short ES at Open and exit at Close at 1900 HKT for that 1 min (done: 28/5)
* add each minute performance graph

### Strategy 3: 2 bars method on both es and hi
* observe first 2 bars (5min) every day
* if 2nd bar close > 1st bar high, long till day end
* if 2nd bar close < 1st bar low, short till day end (30/5)
* regular trading hours (hi: 0915-1630; es: before (0830 - 1500) & after (0930 - 1600) daylight saving time)
* hi: (done: 30/5), es (done: 30/5)

#### Modification:
* ???

## Data:
* hi1 es1 1min ms 20200527.xlsx
* Time-frame: 2019-10-27 to 2020-05-27

## Procedures:
* Run main.py

## Backtest Results:
###  backtest_charts
* Strategy 1: ./backtest_charts/strategy1/strategy1_entry_close_exit_close.png
* Strategy 1: ./backtest_charts/strategy1/strategy1_entry_at_open.png
* Strategy 1: ./backtest_charts/strategy1/sharpe_ratio_heatmap.png
* Strategy 2: ./backtest_charts/strategy2/strategy2_long_only.png
* Strategy 2: ./backtest_charts/strategy2/strategy2_long_only_entry_at_open.png
* Strategy 2: ./backtest_charts/strategy2/strategy2_short_only.png
* Strategy 2: ./backtest_charts/strategy2/strategy2_short_only_entry_at_open.png
* Strategy 2: ./backtest_charts/strategy2/strategy2_long_at_1200.png
* Strategy 2: ./backtest_charts/strategy2/strategy2_short_at_1900.png
* Strategy 3: ./backtest_charts/strategy3/strategy3_long_hi_0925_1630.png
* Strategy 3: ./backtest_charts/strategy3/strategy3_long_hi_0940_1630.png
* Strategy 3: ./backtest_charts/strategy3/strategy3_short_hi_0925_1630.png
* Strategy 3: ./backtest_charts/strategy3/strategy3_short_hi_0940_1630.png
* Strategy 3: ./backtest_charts/strategy3/strategy3_long_short_hi_0925_1630.png
* Strategy 3: ./backtest_charts/strategy3/strategy3_long_short_hi_0940_1630.png
* Strategy 3: ./backtest_charts/strategy3/strategy3_long_short_es.png
* Strategy 3: ./backtest_charts/strategy3/strategy3_long_only_es.png
* Strategy 3: ./backtest_charts/strategy3/strategy3_short_only_es.png

## Appendix
* Daylight Saving Time
* European countries: starts from the last sunday of March & ends at the last sunday of October
* US: starts from the 2nd sunday of March & ends at the first sunday of October
* https://www.market-clock.com/markets/cme/futures/es/

## Remark
* Duplication of data
